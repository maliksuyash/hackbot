import { ActionTypes, CardFactory, TurnContext, TextFormatTypes, TeamsInfo } from "botbuilder";
import { ComponentDialog, DialogContext } from "botbuilder-dialogs";

export class RootDialog extends ComponentDialog {
  constructor(id: string) {
    super(id);
  }

  async onBeginDialog(innerDc: DialogContext, options: {} | undefined) {
    const result = await this.triggerCommand(innerDc);
    if (result) {
      return result;
    }

    return await super.onBeginDialog(innerDc, options);
  }

  async onContinueDialog(innerDc: DialogContext) {
    return await super.onContinueDialog(innerDc);
  }

  async triggerCommand(innerDc: DialogContext) {
    var organiser = ''
    var attendees = ''
    var startTime = ''
    var endTime = ''
    var text = ''
    const removedMentionText = TurnContext.removeRecipientMention(innerDc.context.activity);
    var text = removedMentionText?.replace(/\n|\r/g, "").trim(); // Remove the line break
    if(innerDc.context.activity.value !== undefined && innerDc.context.activity.value !== null)
    {
      if(innerDc.context.activity.value.startTime !== undefined && innerDc.context.activity.value.startTime !== null
        && innerDc.context.activity.value.endTime !== null && innerDc.context.activity.value.endTime !== null)
      {
        startTime = innerDc.context.activity.value.startTime;
        endTime = innerDc.context.activity.value.endTime;
        text = "selectroom"
      }
    }
    else
    {
      var split = text.split("/", 4);
      if(split.length > 3 && split[0] == "BookRoom")
      {
        text = "bookroom";
        var continuationToken;
          var members = [];

          do {
              var pagedMembers = await TeamsInfo.getPagedMembers(innerDc.context, 100, continuationToken);
              continuationToken = pagedMembers.continuationToken;
              members.push(pagedMembers.members);
              members[0].forEach(function (value) {
                if(value.aadObjectId == innerDc.context.activity.from.aadObjectId)
                {
                  organiser = value.name;
                }
                else
                {
                  attendees += value.name + ", ";
                }
              });
          }
          while(continuationToken !== undefined)
      }
      if (innerDc.context.activity.textFormat !== TextFormatTypes.Plain) {
        return await innerDc.cancelAllDialogs();
      }
    }

    switch (text.toLocaleLowerCase()) {
      case "book a room": {
        const card = CardFactory.adaptiveCard({
          "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
          "type": "AdaptiveCard",
          "version": "1.0",
          "body": [
              {
                 "type": "Input.Text",
                 "placeholder": "Enter start time in hh:mm format",
                 "id": "startTime",
              },
              {
                "type": "Input.Text",
                "placeholder": "Enter end time in hh:mm format",
                "id": "endTime",
             }
          ],
          "actions": [
              {
                 "type": "Action.Submit",
                 "title": "OK"
              }
          ]
        });

        await innerDc.context.sendActivity({ attachments: [card] });
        return await innerDc.cancelAllDialogs();
        
      }
      case "selectroom": {
        const cardButtons = [
          {
            type: ActionTypes.ImBack,
            title: "Room 1",
            value: "BookRoom/Room 1/" + startTime + "/" + endTime,
          },
          {
            type: ActionTypes.ImBack,
            title: "Room 2",
            value: "BookRoom/Room 2/" + startTime + "/" + endTime,
          },
          {
            type: ActionTypes.ImBack,
            title: "Room 3",
            value: "BookRoom/Room 3/" + startTime + "/" + endTime,
          },
          {
            type: ActionTypes.ImBack,
            title: "Room 4",
            value: "BookRoom/Room 4/" + startTime + "/" + endTime,
          },
          {
            type: ActionTypes.ImBack,
            title: "Room 5",
            value: "BookRoom/Room 5/" + startTime + "/" + endTime,
          },
        ];
        const card = CardFactory.heroCard("Select room", null, cardButtons, {
          text: `Please click a room to book meeting.`,
        });

        await innerDc.context.sendActivity({ attachments: [card] });
        return await innerDc.cancelAllDialogs();        
      }
      case "bookroom": {
        const card = CardFactory.heroCard("Room Booked", null, null, {
          text: "Room : " + split[1] + "<br>Time : " + split[2] + " - " + split[3]  + "<br>Organiser : " + organiser + "<br>Attendees : " + attendees,
        });

        await innerDc.context.sendActivity({ attachments: [card] });
        return await innerDc.cancelAllDialogs();
      }
      case "show": {
        if (innerDc.context.activity.conversation.isGroup) {
          await innerDc.context.sendActivity(
            `Sorry, currently TeamsFX SDK doesn't support Group/Team/Meeting Bot SSO. To try this command please install this app as Personal Bot and send "show".`
          );
          return await innerDc.cancelAllDialogs();
        }
        break;
      }
      case "intro": {
        const cardButtons = [
          {
            type: ActionTypes.ImBack,
            title: "Show profile",
            value: "show",
          },
        ];
        const card = CardFactory.heroCard("Introduction", null, cardButtons, {
          text: `This Bot has implemented single sign-on (SSO) using the identity of the user signed into the Teams client. See the <a href="https://aka.ms/teamsfx-docs-auth">TeamsFx authentication document</a> and code in <pre>bot/dialogs/mainDialog.js</pre> to learn more about SSO.<br>Type <strong>show</strong> or click the button below to show your profile by calling Microsoft Graph API with SSO. To learn more about building Bot using Microsoft Teams Framework, please refer to the <a href="https://aka.ms/teamsfx-docs">TeamsFx documentation</a>.`,
        });

        await innerDc.context.sendActivity({ attachments: [card] });
        return await innerDc.cancelAllDialogs();
      }
      default: {
        const cardButtons = [
          {
            type: ActionTypes.ImBack,
            title: "Show introduction card",
            value: "intro",
          },
        ];
        const card = CardFactory.heroCard("", null, cardButtons, {
          text: `This is a hello world Bot built with Microsoft Teams Framework, which is designed for illustration purposes. This Bot by default will not handle any specific question or task.<br>Please type <strong>intro</strong> to see the introduction card.`,
        });
        await innerDc.context.sendActivity({ attachments: [card] });
        return await innerDc.cancelAllDialogs();
      }
    }
  }
}
